package nl.demo.giphy.fragments;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.load.resource.gif.GifDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import nl.demo.giphy.R;
import nl.demo.giphy.models.response.GiphyConstants;
import nl.demo.giphy.models.response.GiphyImage;
import nl.demo.giphy.viewmodels.DetailViewModel;
import nl.demo.giphy.viewmodels.MainViewModel;
import nl.demo.giphy.viewmodels.TimerViewModel;

/**
 * The DetailFragment initially shows the selected images (from MainViewModel, shared within the Activity)
 * Start a 10s Timer once the Image is loaded into the ImageView
 * When the Timer runs out loads a new Image and schedules a new Timer on Image load
 */
public class DetailFragment extends Fragment {
    private static final String TAG = "DetailFragment";

    private MainViewModel mMainViewModel;
    private DetailViewModel mDetailViewModel;
    private TimerViewModel mTimerViewModel;

    private ImageView mImageView;
    private TextView mTitleTextView;
    private TextView mUserTextView;
    private ProgressBar mProgressBar;

    public DetailFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mMainViewModel = ViewModelProviders.of(getActivity()).get(MainViewModel.class);
        this.mDetailViewModel = ViewModelProviders.of(this).get(DetailViewModel.class);
        this.mTimerViewModel = ViewModelProviders.of(this).get(TimerViewModel.class);

        // Observe the timer, so the image is refreshed when the timer is emitting (Does NOT start the timer)
        mTimerViewModel.getObservableTimer().observe(this, new Observer<Void>() {
            @Override
            public void onChanged(Void aVoid) {
                refreshImage();
            }
        });

        // Observe the selected image so the image is correctly loaded
        mMainViewModel.getSelectedImage().observe(this, new Observer<GiphyImage>() {
            @Override
            public void onChanged(GiphyImage giphyImage) {
                handleImage(giphyImage);
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_detail, container, false);
        this.mImageView = view.findViewById(R.id.imageView);
        this.mTitleTextView = view.findViewById(R.id.titleTextView);
        this.mUserTextView = view.findViewById(R.id.userTextView);
        this.mProgressBar = view.findViewById(R.id.progressBar);
        return view;
    }

    /**
     * Updates the visibility of the ProgressView & Textviews
     * @param {boolean} loading - If true, shows the loading indicator otherwise hides it and shows textviews
     */
    private void updateVisibility(boolean loading) {
        mProgressBar.setVisibility(loading ? View.VISIBLE : View.GONE);
        mTitleTextView.setVisibility(loading ? View.GONE : View.VISIBLE);
        mUserTextView.setVisibility(loading ? View.GONE : View.VISIBLE);
    }

    /**
     * Handles a new Giphy image, updates the ImageView
     * @param {GiphyImage} image
     */
    private void handleImage(final GiphyImage image) {
        updateVisibility(true);
        if (image.getType().equals(GiphyConstants.IMAGE_TYPE_GIF)) {
            Glide.with(DetailFragment.this)
                    .asGif()
                    .load(image.getOriginalUrl())
                    .listener(new RequestListener<GifDrawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<GifDrawable> target, boolean isFirstResource) {
                            Toast.makeText(getContext(), getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
                            Log.e(TAG, "onLoadFailed: " + e.toString());
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GifDrawable resource, Object model, Target<GifDrawable> target, DataSource dataSource, boolean isFirstResource) {
                            // When the GIF is loaded, update the TextViews & start a timer to refresh the Image after 10 seconds
                            mTitleTextView.setText(image.getTitle());
                            mUserTextView.setText(image.getUserDisplayName());
                            updateVisibility(false);
                            mTimerViewModel.startTimer();
                            return false;
                        }
                    })
                    .into(mImageView);
        } else {
            Toast.makeText(getContext(), getString(R.string.unsupported_format), Toast.LENGTH_SHORT).show();
            this.refreshImage();
        }
    }

    /**
     * Gets a random image from Giphy
     */
    private void refreshImage() {
        mDetailViewModel.getRandomImage().observe(this, new Observer<GiphyImage>() {
            @Override
            public void onChanged(final GiphyImage giphyImage) {
                if (giphyImage == null) {
                    Log.e(TAG, "onChanged: getRandomImage returned null");
                    Toast.makeText(getContext(), R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
                } else {
                    handleImage(giphyImage);
                }
            }
        });
    }
}
