package nl.demo.giphy.fragments;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.List;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import nl.demo.giphy.R;
import nl.demo.giphy.adapters.GiphyImageAdapter;
import nl.demo.giphy.models.response.GiphyImage;
import nl.demo.giphy.viewmodels.MainViewModel;

/**
 * The MainFragment hosts the RecyclerView showing all the Trending Images
 * It updates the MainViewModel when an image is clicked (OnClickListener)
 * It requests new data when the RecyclerView needs more data (OnNearEndOfDataListener)
 */
public class MainFragment extends Fragment implements GiphyImageAdapter.OnClickListener, GiphyImageAdapter.OnNearEndOfDataListener {

    private static final String TAG = "MainFragment";
    public static final String FRAGMENT_TAG = "MAIN_FRAGMENT";

    private MainViewModel mMainViewModel;

    private GiphyImageAdapter mAdapter;
    private RecyclerView mRecyclerView;

    public MainFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mMainViewModel = ViewModelProviders.of(getActivity()).get(MainViewModel.class);
        mMainViewModel.getImages().observe(this, new Observer<List<GiphyImage>>() {
            @Override
            public void onChanged(List<GiphyImage> giphyImages) {
                if (giphyImages == null) {
                    Log.e(TAG, "onChanged: Loading images returned null");
                    Toast.makeText(getContext(), R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
                } else {
                    updateAdapter(giphyImages);
                }
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_main, container, false);

        this.mAdapter = new GiphyImageAdapter();
        this.mAdapter.setOnClickListener(this);
        this.mAdapter.setOnNearEndOfDataListener(this);

        this.mRecyclerView = view.findViewById(R.id.list);
        this.mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        this.mRecyclerView.setAdapter(this.mAdapter);

        return view;
    }

    /**
     * Updates the RecyclerView adapter with new images
     * @param {List<GiphyImage>} giphyImages
     */
    private void updateAdapter(List<GiphyImage> giphyImages) {
        this.mAdapter.addItems(giphyImages);
    }

    /**
     * Handles RecyclerView clicks
     * Updates the selected image on the MainViewModel (Hide this fragment & shows the DetailFragment)
     * @param {GiphyImage} image
     */
    @Override
    public void onItemClicked(GiphyImage image) {
        this.mMainViewModel.setSelectedImage(image);
    }

    /**
     * Handles near end of data events
     * Requests new data through the MainViewModel
     * @param {int} currentItemCount
     */
    @Override
    public void onNearEndOfData(int currentItemCount) {
        mMainViewModel.addOffset(currentItemCount);
    }
}
