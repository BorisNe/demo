package nl.demo.giphy.viewmodels;

import java.util.Timer;
import java.util.TimerTask;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

/**
 * Reusable TimerViewModel, used by the DetailFragment to refresh images
 */
public class TimerViewModel extends ViewModel {
    private static final int REFRESH_TIME = 10000;

    private Timer mTimer = new Timer();
    private MutableLiveData<Void> mObservableTimer = new MutableLiveData<>();

    public void startTimer() {
        this.mTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                mObservableTimer.postValue(null);
            }
        }, REFRESH_TIME);
    }

    public LiveData<Void> getObservableTimer() {
        return mObservableTimer;
    }

}
