package nl.demo.giphy.viewmodels;

import java.util.ArrayList;
import java.util.List;

import androidx.arch.core.util.Function;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;
import nl.demo.giphy.models.response.GiphyConstants;
import nl.demo.giphy.models.response.GiphyImage;
import nl.demo.giphy.models.response.GiphyTrending;
import nl.demo.giphy.repositories.GiphyRepository;

/**
 * The MainViewModel is responsible for storing the Trending Image data & Selected Image
 * Shared by the Fragments in the MainActivity
 */
public class MainViewModel extends ViewModel {
    private GiphyRepository mGiphyRepository;
    private ArrayList<GiphyTrending> bufferedPages = new ArrayList<>();

    private MutableLiveData<Integer> pageIndex = new MutableLiveData<>();
    private MutableLiveData<GiphyImage> selectedImage = new MutableLiveData<>();

    private LiveData<GiphyTrending> lastPage = Transformations.switchMap(pageIndex, new Function<Integer, LiveData<GiphyTrending>>() {
        @Override
        public LiveData<GiphyTrending> apply(Integer page) {
            return mGiphyRepository.getImages(page);
        }
    });
    private LiveData<List<GiphyImage>> images = Transformations.map(lastPage, new Function<GiphyTrending, List<GiphyImage>>() {
        @Override
        public List<GiphyImage> apply(GiphyTrending lastPage) {
            bufferedPages.add(lastPage);

            ArrayList<GiphyImage> images = new ArrayList<>();
            for (GiphyTrending page : bufferedPages) {
                if (page == null) {
                    return null;
                }
                images.addAll(page.data);
            }
            return images;
        }
    });

    public MainViewModel() {
        this.mGiphyRepository = new GiphyRepository();
        this.pageIndex.setValue(0);
    }

    public LiveData<List<GiphyImage>> getImages() {
        return this.images;
    }

    public void addOffset(int currentItemCount) {
        if ((this.pageIndex.getValue() + 1) * GiphyConstants.PAGE_SIZE == currentItemCount) {
            this.pageIndex.setValue(this.pageIndex.getValue() + 1);
        }
    }

    public void setSelectedImage(GiphyImage image) {
        this.selectedImage.setValue(image);
    }

    public LiveData<GiphyImage> getSelectedImage() {
        return this.selectedImage;
    }

}
