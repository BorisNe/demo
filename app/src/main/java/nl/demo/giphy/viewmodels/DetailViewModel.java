package nl.demo.giphy.viewmodels;

import androidx.arch.core.util.Function;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;
import nl.demo.giphy.models.response.GiphyConstants;
import nl.demo.giphy.models.response.GiphyImage;
import nl.demo.giphy.models.response.GiphyRandom;
import nl.demo.giphy.repositories.GiphyRepository;

/**
 * ViewModel specific to the DetailFragment
 */
public class DetailViewModel extends ViewModel {

    private GiphyRepository mGiphyRepository;

    public DetailViewModel() {
        this.mGiphyRepository = new GiphyRepository();
    }

    /**
     * Gets a Random image from the Giphy API
     * @return {LiveData<GiphyImage>}
     */
    public LiveData<GiphyImage> getRandomImage() {
        String tag = Math.random() < 0.3 ? GiphyConstants.TAG : null;

        return Transformations.map(mGiphyRepository.getRandom(tag), new Function<GiphyRandom, GiphyImage>() {
            @Override
            public GiphyImage apply(GiphyRandom input) {
                if (input != null && input.data != null) {
                    return input.data;
                }
                return null;
            }
        });
    }

}
