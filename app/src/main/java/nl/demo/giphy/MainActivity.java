package nl.demo.giphy;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import nl.demo.giphy.fragments.DetailFragment;
import nl.demo.giphy.fragments.MainFragment;
import nl.demo.giphy.models.response.GiphyImage;
import nl.demo.giphy.viewmodels.MainViewModel;

/**
 * The MainActivity hosts the MainFragment & DetailFragment
 * Observes the MainViewModel and responds to changes to the Selected Image
 * When the Selected Image changes it adds a new DetailFragment to the layout
 */
public class MainActivity extends AppCompatActivity {

    private String TAG = "MainActivity";
    private MainViewModel mMainViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Get the MainViewModel and observe the selectedImage to show the DetailFragment when an image is selected
        mMainViewModel = ViewModelProviders.of(this).get(MainViewModel.class);
        mMainViewModel.getSelectedImage().observe(this, new Observer<GiphyImage>() {
            @Override
            public void onChanged(GiphyImage giphyImage) {
                showDetailFragment();
            }
        });

        // Add main fragment to layout
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.frame, new MainFragment(), MainFragment.FRAGMENT_TAG)
                .commit();
    }

    /**
     * Hides the MainFragment & Adds a DetailFragment to the Activity layout
     */
    private void showDetailFragment() {
        Fragment mainFragment = getSupportFragmentManager().findFragmentByTag(MainFragment.FRAGMENT_TAG);

        getSupportFragmentManager()
                .beginTransaction()
                .hide(mainFragment)
                .add(R.id.frame, new DetailFragment())
                .addToBackStack(null)
                .commit();
    }

}
