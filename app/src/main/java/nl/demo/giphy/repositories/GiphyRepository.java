package nl.demo.giphy.repositories;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import nl.demo.giphy.models.response.GiphyConstants;
import nl.demo.giphy.models.response.GiphyRandom;
import nl.demo.giphy.models.response.GiphyTrending;
import nl.demo.giphy.retrofit.GiphyService;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.internal.EverythingIsNonNull;

/**
 * The GiphyRepository wraps the GiphyService (Retrofit)
 */
public class GiphyRepository {

    private GiphyService giphyService;

    public GiphyRepository() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BASIC);

        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(logging)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(GiphyConstants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        this.giphyService = retrofit.create(GiphyService.class);
    }

    /**
     * Gets a page of trending images from Giphy
     * @param {int} page
     * @return {LiveData<GiphyTrending>}
     */
    public LiveData<GiphyTrending> getImages(int page) {
        final MutableLiveData<GiphyTrending> mutableLiveData = new MutableLiveData<>();

        giphyService.getTrending(page * GiphyConstants.PAGE_SIZE, GiphyConstants.PAGE_SIZE).enqueue(new Callback<GiphyTrending>() {

            @Override
            @EverythingIsNonNull
            public void onResponse(Call<GiphyTrending> call, Response<GiphyTrending> response) {
                if (response.body() != null) {
                    mutableLiveData.setValue(response.body());
                } else {
                    mutableLiveData.setValue(null);
                }
            }

            @Override
            @EverythingIsNonNull
            public void onFailure(Call<GiphyTrending> call, Throwable t) {
                mutableLiveData.setValue(null);
            }
        });

        return mutableLiveData;
    }

    /**
     * Gets a Random image from the Giphy Random API
     * @return {LiveData<GiphyRandom>}
     */
    public LiveData<GiphyRandom> getRandom(@Nullable String tag) {
        final MutableLiveData<GiphyRandom> mutableLiveData = new MutableLiveData<>();

        giphyService.getRandom(tag).enqueue(new Callback<GiphyRandom>() {

            @Override
            @EverythingIsNonNull
            public void onResponse(Call<GiphyRandom> call, Response<GiphyRandom> response) {
                if (response.body() != null) {
                    mutableLiveData.setValue(response.body());
                } else {
                    mutableLiveData.setValue(null);
                }
            }

            @Override
            @EverythingIsNonNull
            public void onFailure(Call<GiphyRandom> call, Throwable t) {
                mutableLiveData.setValue(null);
            }
        });

        return mutableLiveData;
    }

}
