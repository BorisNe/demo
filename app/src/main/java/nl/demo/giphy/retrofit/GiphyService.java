package nl.demo.giphy.retrofit;

import nl.demo.giphy.models.response.GiphyRandom;
import nl.demo.giphy.models.response.GiphyTrending;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

public interface GiphyService {

    @Headers({
            "api_key: dc6zaTOxFJmzC"
    })
    @GET("v1/gifs/trending")
    Call<GiphyTrending> getTrending(@Query("offset") int offset, @Query("limit") int limit);

    @Headers({
            "api_key: dc6zaTOxFJmzC"
    })
    @GET("v1/gifs/random")
    Call<GiphyRandom> getRandom(@Query("tag") String tag);

}
