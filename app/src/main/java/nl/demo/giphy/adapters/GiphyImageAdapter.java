package nl.demo.giphy.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import nl.demo.giphy.R;
import nl.demo.giphy.models.response.GiphyConstants;
import nl.demo.giphy.models.response.GiphyImage;

public class GiphyImageAdapter extends RecyclerView.Adapter<GiphyImageViewHolder>{

    private static final int ITEMS_TO_END = 10;

    private List<GiphyImage> items;
    private OnClickListener clickListener;
    private OnNearEndOfDataListener nearEndOfDataListener;

    /**
     * Adds Giphy Images to the adapter, appends them to the current items
     * Notifies the Adapter / RecyclerView of new inserted items
     * @param items
     */
    public void addItems(List<GiphyImage> items) {
        int oldSize = 0;
        if (this.items != null) {
            oldSize = this.items.size();
        }
        this.items = items;
        notifyItemRangeInserted(oldSize, items.size());
    }

    @NonNull
    @Override
    public GiphyImageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_image, parent, false);
        return new GiphyImageViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull GiphyImageViewHolder holder, int position) {
        final GiphyImage image = this.items.get(position);

        holder.titleTextView.setText(image.getTitle());
        holder.userTextView.setText(image.getUserDisplayName());

        int color = position % 2 == 0 ? R.color.itemBackground : R.color.itemBackgroundDark;
        holder.itemView.setBackgroundColor(holder.itemView.getContext().getColor(color));

        if (image.getType().equals(GiphyConstants.IMAGE_TYPE_GIF)) {
            Glide.with(holder.imageView)
                    .asGif()
                    .load(image.getPreviewUrl())
                    .into(holder.imageView);
        } else {
            // TODO: Optionally handle other formats
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (clickListener != null) {
                    clickListener.onItemClicked(image);
                }
            }
        });

        if (nearEndOfDataListener != null && position >= this.items.size() - ITEMS_TO_END) {
            nearEndOfDataListener.onNearEndOfData(this.items.size());
        }
    }

    @Override
    public int getItemCount() {
        if (this.items == null) {
            return 0;
        }
        return this.items.size();
    }

    /**
     * Sets the OnClickListener
     * @param {OnClickListener} listener
     */
    public void setOnClickListener(OnClickListener listener) {
        this.clickListener = listener;
    }

    /**
     * Sets the OnNearEndOfDataListener
     * @param {OnNearEndOfDataListener} listener
     */
    public void setOnNearEndOfDataListener(OnNearEndOfDataListener listener) {
        this.nearEndOfDataListener = listener;
    }

    public interface OnClickListener {
        void onItemClicked(GiphyImage image);
    }

    public interface OnNearEndOfDataListener {
        void onNearEndOfData(int currentItemCount);
    }

}
