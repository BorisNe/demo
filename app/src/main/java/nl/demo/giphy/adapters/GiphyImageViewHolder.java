package nl.demo.giphy.adapters;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import nl.demo.giphy.R;

public class GiphyImageViewHolder extends RecyclerView.ViewHolder {

    public TextView titleTextView;
    public TextView userTextView;
    public ImageView imageView;

    public GiphyImageViewHolder(@NonNull View itemView) {
        super(itemView);
        this.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        this.titleTextView = itemView.findViewById(R.id.textView);
        this.userTextView = itemView.findViewById(R.id.textView2);
        this.imageView = itemView.findViewById(R.id.imageView);
    }
}
