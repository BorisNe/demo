package nl.demo.giphy.models.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GiphyRandom {

    @SerializedName("data")
    public GiphyImage data;

}
