package nl.demo.giphy.models.response;

import com.google.gson.annotations.SerializedName;

public class GiphyImageSources {

    @SerializedName("preview_gif")
    private GiphyImageSource previewGif;

    @SerializedName("original")
    private GiphyImageSource original;

    public String getPreviewUrl() {
        return previewGif.url;
    }

    public String getOriginalUrl() {
        return this.original.url;
    }

    private class GiphyImageSource {
        private String url;
    }
}
