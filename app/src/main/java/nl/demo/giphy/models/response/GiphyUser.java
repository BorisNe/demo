package nl.demo.giphy.models.response;

import com.google.gson.annotations.SerializedName;

public class GiphyUser {

    @SerializedName("display_name")
    private String displayName;

    public String getDisplayName() {
        return displayName;
    }
}
