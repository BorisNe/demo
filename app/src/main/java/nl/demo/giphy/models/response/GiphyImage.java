package nl.demo.giphy.models.response;

import com.google.gson.annotations.SerializedName;

public class GiphyImage {

    @SerializedName("type")
    private String type;

    @SerializedName("title")
    private String title;

    @SerializedName("user")
    private GiphyUser user;

    @SerializedName("images")
    private GiphyImageSources images;

    public String getType() {
        return this.type;
    }

    public String getTitle() {
        return this.title;
    }

    public String getUserDisplayName() {
        if (this.user != null) {
            return this.user.getDisplayName();
        } else {
            return null;
        }
    }

    public String getPreviewUrl() {
        return this.images.getPreviewUrl();
    }

    public String getOriginalUrl() {
        return this.images.getOriginalUrl();
    }

}
