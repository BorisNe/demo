package nl.demo.giphy.models.response;

public class GiphyConstants {

    public static final String BASE_URL = "https://api.giphy.com";
    public static final String IMAGE_TYPE_GIF = "gif";
    public static final int PAGE_SIZE = 20;
    public static final String TAG = "hackerman";

}
