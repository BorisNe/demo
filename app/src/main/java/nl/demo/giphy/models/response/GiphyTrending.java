package nl.demo.giphy.models.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GiphyTrending {

    @SerializedName("data")
    public List<GiphyImage> data;

    @SerializedName("pagination")
    public GiphyPagination pagination;

}
