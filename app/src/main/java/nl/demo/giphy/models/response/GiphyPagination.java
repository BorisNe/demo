package nl.demo.giphy.models.response;

import com.google.gson.annotations.SerializedName;

public class GiphyPagination {

    @SerializedName("total_count")
    public int totalCount;

    @SerializedName("count")
    public int count;

    @SerializedName("offset")
    public int offset;

}
